if (Drupal.jsEnabled) {
  $(document).ready( function() {
    $('#edit-output-settings-entries-auto-slide-autoSlide').click(function(){
      if (this.checked==true){
        $('#edit-output-settings-entries-auto-slide-autoSlideInterval').removeAttr('disabled');
        if($('#edit-output-settings-entries-auto-slide-autoSlideInterval').val()=='') $('#edit-output-settings-entries-auto-slide-autoSlideInterval').val('3000');
        $('#edit-output-settings-entries-auto-slide-delayAutoSlide').removeAttr('disabled');
        if($('#edit-output-settings-entries-auto-slide-delayAutoSlide').val()=='') $('#edit-output-settings-entries-auto-slide-delayAutoSlide').val('0');
      }
      else {
        $('#edit-output-settings-entries-auto-slide-autoSlideInterval').attr('disabled', 'disabled');
        $('#edit-output-settings-entries-auto-slide-delayAutoSlide').attr('disabled', 'disabled');
      }
    });
    $('#edit-output-settings-entries-effects-effect').click(function(){
      if ($(this).val()=='slide') {
        $('#edit-output-settings-entries-effects-slideEasing').removeAttr('disabled');
        $('#edit-output-settings-entries-effects-animSpeed').removeAttr('disabled');
      }
      else {
        $('#edit-output-settings-entries-effects-slideEasing').attr('disabled', 'disabled');
        $('#edit-output-settings-entries-effects-animSpeed').attr('disabled', 'disabled');
      }
    });
  });
}