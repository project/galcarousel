<?php

/**
 * @file
 * Provides administrative functions for galcarousel module.
 */

function galcarousel_galleryapi_carousel_form() {
  drupal_add_js(drupal_get_path('module', 'galcarousel') .'/galcarousel.admin.js');
  $pid=arg(3);
  if (is_numeric($pid)) {
    $preset=galleryapi_preset_load(arg(3));
  }
  $form['general'] = array(
    '#type' => 'fieldset',
    '#title' => t('General settings'),
    '#collapsible' => TRUE,
  );
  $form['general']['direction'] = array(
    '#type' => 'select',
    '#title' => t('Slide direction'),
    '#options' => array('horizontal' => t('Horizontal'), 'vertical' => t('Vertical')),
    '#default_value' => 'horizontal',
  );
  $form['general']['loop'] = array(
    '#type' => 'checkbox',
    '#title' => t('Loop carousel'),
    '#default_value' => 0,
    '#description' => t('Check for loop the carousel.'),
  );
  $form['general']['dispItems'] = array(
    '#type' => 'textfield',
    '#title' => t('Display items'),
    '#default_value' => 1,
    '#description' => t('Number of items to display per step.'),
    '#size' => 10,
  );
  $form['general']['width'] = array(
    '#type' => 'textfield',
    '#title' => t('Slide item width in px'),
    '#default_value' => '',
    '#size' => 10,
  );
  $form['navigation'] = array(
    '#type' => 'fieldset',
    '#title' => t('Navigation'),
    '#collapsible' => TRUE,
  );
  $form['navigation']['pagination'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable pagination'),
    '#default_value' => 0,
  );
  $form['navigation']['nextBtn'] = array(
    '#type' => 'textfield',
    '#title' => t('Next button item html'),
    '#default_value' => '',
    '#size' => 10,
  );
  $form['navigation']['prevBtn'] = array(
    '#type' => 'textfield',
    '#title' => t('Previous button item html'),
    '#default_value' => '',
    '#size' => 10,
  );
  $form['auto_slide'] = array(
    '#type' => 'fieldset',
    '#title' => t('Auto slide'),
    '#collapsible' => TRUE,
  );
  $form['auto_slide']['autoSlide'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable automatic slide'),
    '#default_value' => $preset['output_data']['auto_slide']['autoSlide'],
  );
   $form['auto_slide']['autoSlideInterval'] = array(
    '#type' => 'textfield',
    '#title' => t('Automatic slide interval, ms'),
    '#default_value' => '3000',
    '#description' => t('Determines delay (in ms) between automatic slide.'),
    '#size' => 5,
    '#disabled' => (!$preset['output_data']['auto_slide']['autoSlide']),
  );
  $form['auto_slide']['delayAutoSlide'] = array(
    '#type' => 'textfield',
    '#title' => t('Automatic slide delay, ms'),
    '#default_value' => '0',
    '#description' => t('Determines time delay between two co-existing carousels (in ms).'),
    '#size' => 5,
    '#disabled' => (!$preset['output_data']['auto_slide']['autoSlide']),
  );
  $form['effects'] = array(
    '#type' => 'fieldset',
    '#title' => t('Effecs & Compatibility'),
    '#collapsible' => TRUE,
  );
  $form['effects']['effect'] = array(
    '#type' => 'select',
    '#title' => t('Effect for animation'),
    '#options' => array('slide' => t('Slide'), 'fade' => t('Fade')),
    '#default_value' => 'slide',
  );
  if ($preset['output_data']['effects']['effect']==='fade') $disabled=TRUE;
  $form['effects']['slideEasing'] = array(
    '#type' => 'select',
    '#title' => t('Easing effect for slide animation'),
    '#options' => array('none' => t('None'), 'swing' => t('Swing'), 'easeInBounce' => t('In Bounce'), 'easeOutBounce' => t('Out Bounce'), 'easeInOutBounce' => t('In-Out Bounce')),
    '#default_value' => 'none',
    '#disabled' => $disabled,
  );
  $form['effects']['animSpeed'] = array(
    '#type' => 'select',
    '#title' => t('Animation speed for slide animation'),
    '#options' => array('slow' => t('Slow'), 'normal' => t('Normal'), 'fast' => t('Fast')),
    '#default_value' => 'normal',
    '#disabled' => $disabled,
  );
  $form['effects']['combinedClass'] = array(
    '#type' => 'checkbox',
    '#title' => t('Emulate multiple classes in IE6.'),
    '#default_value' => 0,
  );
  return $form;
}